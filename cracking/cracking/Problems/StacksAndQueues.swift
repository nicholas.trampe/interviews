//
//  StacksAndQueues.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/15/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

// 3.2

struct MinStack<T: Comparable>: Stackable {
  private var stack = Stack<T>()
  var min: T? = nil
  
  mutating func pop() -> T? {
    return stack.pop()
  }
  
  mutating func push(_ item: T) {
    if let current = min {
      if item < current {
        min = item
      }
    } else {
      min = item
    }
    
    stack.push(item)
  }
  
  func peek() -> T? {
    return stack.peek()
  }
  
  var count: Int {
    get {
      return stack.count
    }
  }
  
  var isEmpty: Bool {
    get {
      return stack.isEmpty
    }
  }
}

// 3.3

struct SetOfStacks<T> {
  private var stacks: [Stack<T>] = []
  private var threshold: Int = 10
  
  mutating func pop() -> T? {
    guard !stacks.isEmpty else {
      return nil
    }
    
    if stacks[stacks.count-1].isEmpty {
      return nil
    } else {
      let res = stacks[stacks.count-1].pop()
      if stacks[stacks.count-1].isEmpty {
        stacks.removeLast()
      }
      
      return res
    }
  }
  
  mutating func push(_ item: T) {
    if stacks.isEmpty {
      stacks.append(Stack<T>())
    }
    
    if stacks[stacks.count-1].count == threshold {
      stacks.append(Stack<T>())
    }
    
    stacks[stacks.count-1].push(item)
  }
}

extension SetOfStacks: CustomStringConvertible {
  var description: String {
    get {
      var res = ""
      
      for stack in stacks {
        res += stack.description + "\n"
      }
      
      return res
    }
  }
}

// 3.5

struct SortableStack<T: Comparable>: Stackable {
  private var stack = Stack<T>()
  
  mutating func push(_ item: T) {
    stack.push(item)
  }
  
  mutating func pop() -> T? {
    return stack.pop()
  }
  
  func peek() -> T? {
    return stack.peek()
  }
  
  mutating func sort() {
    var temp = SortableStack<T>()
    
    while let x = pop() {
      while let y = temp.peek(), y > x {
        push(temp.pop()!)
      }
      
      temp.push(x)
    }
    
    while let x = temp.pop() {
      push(x)
    }
  }
  
  var count: Int {
    get {
      return stack.count
    }
  }
  
  var isEmpty: Bool {
    get {
      return stack.isEmpty
    }
  }
}

// 3.6

struct Animal {
  enum AnimalType: String {
    case dog = "Dog"
    case cat = "Cat"
  }
  
  let type: AnimalType
  let name: String
}

extension Animal: CustomStringConvertible {
  var description: String {
    get {
      return "\(name): \(type.rawValue)"
    }
  }
}

struct Shelter: Queueable {
  private var animals: [Animal] = []
  
  mutating func enqueue(_ item: Animal) {
    animals.append(item)
  }
  
  mutating func dequeue() -> Animal? {
    guard !isEmpty else {
      return nil
    }
    
    return animals.removeFirst()
  }
  
  func peek() -> Animal? {
    guard !isEmpty else {
      return nil
    }
    
    return animals.first
  }
  
  mutating func dequeueDog() -> Animal? {
    return dequeue(.dog)
  }
  
  mutating func dequeueCat() -> Animal? {
    return dequeue(.cat)
  }
  
  private mutating func dequeue(_ type: Animal.AnimalType) -> Animal? {
    guard !isEmpty else {
      return nil
    }
    
    if let index = animals.firstIndex(where: { $0.type == type }) {
      return animals.remove(at: index)
    }
    
    return nil
  }
  
  var isEmpty: Bool {
    get {
      return animals.isEmpty
    }
  }
  
  var count: Int {
    get {
      return animals.count
    }
  }
}

extension Shelter: CustomStringConvertible {
  var description: String {
    get {
      return animals.description
    }
  }
}
