//
//  GraphsAndTrees.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/15/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

// 4.1

extension Graph {
  func pathExists(from: Vertex<Element>, to: Vertex<Element>) -> Bool {
    return bfs(start: from, { $0 === to })
  }
}

// 4.2

// See BinarySearchTree init w/ array

// 4.3

extension BinarySearchTree {
  var nodesSeparatedByDepth: [Int:[T]] {
    get {
      var depths: [Int:[T]] = [:]
      buildDepth(depth: 0, result: &depths)
      return depths
    }
  }
  
  private func buildDepth(depth: Int, result: inout [Int:[T]]) {
    result[depth] = (result[depth] ?? []) + [data]
    
    left?.buildDepth(depth: depth+1, result: &result)
    right?.buildDepth(depth: depth+1, result: &result)
  }
}

// 4.4

extension BinarySearchTree {
  var isBalanced: Bool {
    get {
      let leftHeight = left?.height ?? 0
      let rightHeight = right?.height ?? 0
      
      if abs(leftHeight - rightHeight) > 1 {
        return false
      }
      
      return true
    }
  }
  
  var height: Int {
    get {
      return max((left?.height ?? 0) + 1, (right?.height ?? 0) + 1)
    }
  }
}


// 4.5

extension BinarySearchTree where T == Int  {
  var isValid: Bool {
    get {
      return isValid(startingAt: left, range: (T.min, data)) && isValid(startingAt: right, range: (data, T.max))
    }
  }
  
  private func isValid(startingAt node: BinarySearchTree?, range: (min: T, max: T)) -> Bool {
    guard let node = node else {
      return true
    }
    
    if node.data < range.min || node.data > range.max {
      return false
    }
    
    return isValid(startingAt: node.left, range: (range.min, node.data)) && isValid(startingAt: node.right, range: (node.data, range.max))
  }
}

// 4.7

typealias Project = Character
typealias Dependency = (from: Project, to: Project)
enum BuildingError: Error {
  case invalidDependency
  case dependencyCycle
}

func buildOrder(projects: [Project], dependencies: [Dependency]) throws -> [Project] {
  let graph = AdjacencyMatrixGraph<Project>()
  var orderedProjects: [Project] = []
  var verticies: [Project:Vertex<Project>] = [:]
  
  for project in projects {
    verticies[project] = graph.add(element: project)
  }
  
  for dependency in dependencies {
    guard let from = verticies[dependency.from], let to = verticies[dependency.to] else {
      throw BuildingError.invalidDependency
    }
    graph.addDirectedEdge(from: from, to: to, weight: 1)
  }
  
  while graph.edges.count > 0 {
    let openVerticies = graph.verticies.filter({ graph.edges(from: $0).count == 0 && !orderedProjects.contains($0.data) })
    
    guard openVerticies.count > 0 else {
      throw BuildingError.dependencyCycle
    }
    
    orderedProjects += openVerticies.map({$0.data})
    
    openVerticies.forEach({
      graph.removeIncomingEdges(to: $0)
    })
  }
  
  let remaining: [Project] = graph.verticies.map({$0.data}).filter({!orderedProjects.contains($0)})
  orderedProjects += remaining
  
  return orderedProjects
}

//print(buildOrder(projects: ["a","b","c","d","e","f"], dependencies: [(from: "d", to: "a"),(from: "b", to: "f"),(from: "d", to: "b"),(from: "a", to: "f"),(from: "c", to: "d")]))


// 4.10

extension BinarySearchTree {
  
  // Preorder traversal with additional null node identifier
  // Root always comes first in preorder
  // Adding the null 'ø' uniquly identifies leafs
  var uniqueIdentifier: String {
    get {
      return "\(data)" + (left?.uniqueIdentifier ?? "ø") + (right?.uniqueIdentifier ?? "ø")
    }
  }
  
  
  func contains(tree: BinarySearchTree) -> Bool {
    return uniqueIdentifier.contains(tree.uniqueIdentifier)
  }
}

// 4.12

extension BinarySearchTree where T == Int {
  func pathsWithSum(_ sum: Int) -> Int {
    return pathsWithSum(0, sum)
  }
  
  private func pathsWithSum(_ sum: Int, _ target: Int) -> Int {
    var result = 0
    
    if sum == target {
      result += 1
    }
    
    result += left?.pathsWithSum(sum + data, target) ?? 0
    result += right?.pathsWithSum(sum + data, target) ?? 0
    
    result += left?.pathsWithSum(0, target) ?? 0
    result += right?.pathsWithSum(0, target) ?? 0
    
    return result
  }
}

// 4.6

extension BinarySearchTree {
  var successor: BinarySearchTree? {
    if let right = right {
      return right.leftMost
    }
    
    var next: BinarySearchTree? = parent
    
    while let parent = next, parent.data < data {
      next = parent.parent
    }
    
    return next
  }
  
  private var leftMost: BinarySearchTree? {
    get {
      var node = self
      
      while let left = node.left {
        node = left
      }
      
      return node
    }
  }
}


// 4.9

extension BinarySearchTree {
  var sequences: [[T]] {
    get {
      let leftOrdered = left?.preorder ?? []
      let rightOrdered = right?.preorder ?? []
      let permutations = inOrderPermutations(leftOrdered, rightOrdered)
      
      return permutations.map({ [data] + $0 })
    }
  }
  
  private func inOrderPermutations(_ lhs: [T], _ rhs: [T]) -> [[T]] {
    if lhs.count == 0 {
      return [rhs]
    }
    
    if rhs.count == 0 {
      return [lhs]
    }
    
    let firstLeft = lhs[0]
    let permsLeft = inOrderPermutations(Array<T>(lhs[1...]), rhs).map({ [firstLeft] + $0 })
    let firstRight = rhs[0]
    let permsRight = inOrderPermutations(lhs, Array<T>(rhs[1...])).map({ [firstRight] + $0 })
    
    return permsLeft + permsRight
  }
}
