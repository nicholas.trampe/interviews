//
//  Design.swift
//  cracking
//
//  Created by Nicholas Trampe on 9/4/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct Call {
  var dispatcher: Dispatcher? = nil
  
  func start() {}
  func end() {}
}

protocol Dispatcher {
  var phone: String { get }
  var isFree: Bool { get }
  
  func dispatch(call: inout Call)
}

extension Dispatcher {
  func dispatch(call: inout Call) {
    call.dispatcher = self
    
    call.start()
    print("Calling \(phone)")
    call.end()
  }
}

protocol Employee: Dispatcher {
  var id: String { get }
  var level: UInt { get }
}

struct Respondent: Employee {
  let id: String
  let level: UInt = 3
  var phone: String = ""
  var isFree: Bool = true
  
  init(id: String, phone: String, isFree: Bool) {
    self.id = id
    self.phone = phone
    self.isFree = isFree
  }
}

struct Manager: Employee {
  let id: String
  let level: UInt = 2
  var phone: String = ""
  var isFree: Bool = true
  
  init(id: String, phone: String, isFree: Bool) {
    self.id = id
    self.phone = phone
    self.isFree = isFree
  }
}

struct Director: Employee {
  let id: String
  let level: UInt = 1
  var phone: String = ""
  var isFree: Bool = true
  
  init(id: String, phone: String, isFree: Bool) {
    self.id = id
    self.phone = phone
    self.isFree = isFree
  }
}

struct Center {
  var callers: [Employee] = []
  
  func dispatch(call: inout Call) {
    let callersSorted = callers.sorted { (lhs, rhs) -> Bool in
      return lhs.level > rhs.level
    }
    
    let caller: Dispatcher? = callersSorted.first { $0.isFree }
    caller?.dispatch(call: &call)
  }
}


// 7.9

struct CircularArray<T>: RandomAccessCollection, IteratorProtocol {
  typealias Element = T
  private let data: [T]
  private var currentIndex = 0
  var startingIndex = 0
  
  init(_ data: [T]) {
    self.data = data
  }
  
  var startIndex: Array<T>.Index {
    return startingIndex
  }
  var endIndex: Array<T>.Index {
    var index = startingIndex-1
    if index < 0 {
      index = data.count-1
    }
    return index
  }
  
  subscript(position: Int) -> T {
    var index = startingIndex
    for _ in 0..<position {
      index += 1
      
      if index == data.count {
        index = 0
      }
    }
    
    return data[index]
  }
  
  func index(after i: Int) -> Int {
    var index = i + 1
    if index == data.count {
      index = 0
    }
    
    return index
  }
  
  mutating func next() -> Element? {
    guard currentIndex >= 0 && currentIndex < data.count,
      currentIndex != startingIndex else {
        return nil
    }
    
    let element = data[currentIndex]
    
    currentIndex += 1
    if currentIndex == data.count {
      currentIndex = 0
    }
    
    return element
  }
}
