//
//  Moderate.swift
//  cracking
//
//  Created by Nicholas Trampe on 9/4/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation


func englishInt(num: Int) {
  var current = num
  
  if current < 0 {
    print("Negative ")
    current *= -1
  }
  
  while current != 0 {
    print(current % 10)
    
    current /= 10
  }
}


func contSeq(array: [Int]) -> Int {
  var maxSum = array.first ?? 0
  var currentSum = 0
  
  for element in array {
    currentSum += element
    
    if currentSum > maxSum {
      maxSum = currentSum
    }
    
    if currentSum < 0 {
      currentSum = 0
    }
  }
  
  return maxSum
}


enum Operator: Character, CaseIterable, CustomStringConvertible {
  case add = "+"
  case subtract = "-"
  case multiply = "*"
  case divide = "/"
  
  func operate(_ lhs: Double, _ rhs: Double) -> Double {
    switch self {
    case .add:
      return lhs + rhs
    case .subtract:
      return lhs - rhs
    case .multiply:
      return lhs * rhs
    case .divide:
      return lhs / rhs
    }
  }
  
  static var set: Set<Character> {
    get {
      return Set<Character>(Operator.allCases.map({ $0.rawValue }))
    }
  }
  
  var description: String {
    get {
      return "\(rawValue)"
    }
  }
}

func operate(equation: String) -> Double {
  var numbers = Stack<Double>()
  var operators = Stack<Operator>()
  
  var startingIndex = 0
  
  for (index, character) in equation.enumerated() {
    let op = Operator(rawValue: character)
    let startIndex = equation.index(equation.startIndex, offsetBy: startingIndex)
    let endIndex = equation.index(equation.startIndex, offsetBy: index == equation.count-1 ? index+1 : index)
    let num = Double(equation[startIndex..<endIndex])
    
    if let op = op {
      if let num = num {
        numbers.push(num)
      }
      
      if let lastOp = operators.peek(), lastOp == .multiply || lastOp == .divide {
        if let rhs = numbers.pop(), let lhs = numbers.pop() {
          let result = lastOp.operate(lhs, rhs)
          numbers.push(result)
          operators.pop()
        }
      }
      
      operators.push(op)
      startingIndex = index+1
    } else if let num = num, index == equation.count-1 {
      numbers.push(num)
    }
    
    print("numbers: \(numbers)")
    print("operators: \(operators)")
  }
  
  while let op = operators.pop() {
    if let rhs = numbers.pop(), let lhs = numbers.pop() {
      let result = op.operate(lhs, rhs)
      numbers.push(result)
    }
  }
  
  return numbers.pop() ?? 0
}


// 16.20

func t9(_ input: [Int]) -> [String] {
  return t9(input, index: 0, partialWord: "")
}

private func t9(_ input: [Int], index: Int, partialWord: String) -> [String] {
  if index >= input.count {
    return []
  }
  
  if EnglishTrie.words(startingWith: partialWord).count == 0 {
    return []
  }
  
  var result: [String] = []
  let characters = T9KeyboardMap[input[index]]
  
  for character in characters {
    let word = "\(partialWord)\(character)"
    if index == input.count-1 && EnglishTrie.contains(word) {
      result.append(word)
    }
    
    result += t9(input, index: index + 1, partialWord: word)
  }
  
  return result
}
