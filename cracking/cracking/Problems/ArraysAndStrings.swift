//
//  ArraysAndStrings.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/14/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

// 1.1

extension StringProtocol {
  var isUnique: Bool {
    get {
      var table = Set<Character>()
      
      for char in self {
        if table.contains(char) {
          return false
        }
        
        table.insert(char)
      }
      
      return true
    }
  }
}

// 1.2

extension String {
  func isPermutation(_ another: String) -> Bool {
    guard self.count == another.count else {
      return false
    }
    
    var ourCharacters = self.map({$0})
    
    for char in another {
      if let index = ourCharacters.index(of: char) {
        ourCharacters.remove(at: index)
      } else {
        return false
      }
    }
    
    return true
  }
}

// 1.3

extension String {
  func urlify() -> String {
    return self.trimmingCharacters(in: .whitespaces).replacingOccurrences(of: " ", with: "%20")
  }
}

// 1.4

extension String {
  var isPalindromPermutation: Bool {
    get {
      guard self.count > 2 else {
        return true
      }
      
      let strippedStr = self.replacingOccurrences(of: " ", with: "")
      let characterCounts = strippedStr.characterCounts
      let numberOfUnevenCharacters = characterCounts.filter({ $0.value % 2 != 0 }).count
      
      return numberOfUnevenCharacters <= 1
    }
  }
  
  private var characterCounts: [Character:Int] {
    get {
      var counts: [Character:Int] = [:]
      
      for char in self {
        counts[char] = (counts[char] ?? 0) + 1
      }
      
      return counts
    }
  }
}

// 1.5

extension String {
  mutating func replace(_ char: Character, at i: Int) {
    remove(at: index(startIndex, offsetBy: i))
    insert(char, at: index(startIndex, offsetBy: i))
  }
  
  mutating func remove(at i: Int) {
    remove(at: index(startIndex, offsetBy: i))
  }
  
  func isOneAway(_ other: String) -> Bool {
    if self.count != other.count {
      return isOneCountAway(lhs: self, rhs: other)
    }
    
    for (index, char) in self.enumerated() {
      var otherCopy = other
      otherCopy.replace(char, at: index)
      if self == otherCopy {
        return true
      }
    }
    
    return false
  }
  
  private func isOneCountAway(lhs: String, rhs: String) -> Bool {
    let (smaller, larger) = lhs.count > rhs.count ? (rhs, lhs) : (lhs, rhs)
    
    for i in 0..<larger.count {
      var largerCopy = larger
      largerCopy.remove(at: i)
      if smaller == largerCopy {
        return true
      }
    }
    
    return false
  }
}

// 1.6

extension String {
  private typealias CharacterCount = (character: Character, count: Int)
  
  private var characterCountsOrdered: [CharacterCount] {
    get {
      var current: Character = self[startIndex]
      var res: [CharacterCount] = [(character: current, count: 0)]
      
      for char in self {
        if char == current {
          res[res.count-1].count += 1
        } else {
          res.append((character: char, count: 1))
          current = char
        }
      }
      
      return res
    }
  }
  
  var compressed: String {
    get {
      let counts = characterCountsOrdered
      var res = ""
      
      for charCount in counts {
        res += "\(charCount.character)\(charCount.count)"
      }
      
      return (res.count < self.count ? res : self)
    }
  }
}

// 1.7

func rotate(matrix: [[Int]]) -> [[Int]] {
  precondition(matrix.count == matrix.first?.count, "Matrix must be NxN")
  
  var res = Array<[Int]>(repeating: Array<Int>(repeating: 0, count: matrix.count), count: matrix.count)
  
  for i in 0..<matrix.count {
    for j in 0..<matrix.count {
      res[i][j] = matrix[i][matrix.count - 1 - j]
    } 
  }
  
  return res
}

// 1.9

extension String {
  func isRotation(_ str: String) -> Bool {
    for (index, _) in str.enumerated() {
      let currentIndex = str.index(str.startIndex, offsetBy: index)
      let endIndex = str.endIndex
      let remaining = str[currentIndex..<endIndex]
      
      if self.hasPrefix(remaining) {
        let startIndex = str.startIndex
        let rotated = remaining + str[startIndex..<currentIndex]
        return self == rotated
      }
    }
    
    return false
  }
  
  func isRotationOnce(_ str: String) -> Bool {
    return (self + self).contains(str)
  }
}


// Extras

extension StringProtocol {
  var isPalindrome: Bool {
    get {
      var i = 0
      var j = self.count-1
      
      while i < j {
        let leftIndex = self.index(self.startIndex, offsetBy: i)
        let rightIndex = self.index(self.startIndex, offsetBy: j)
        if self[leftIndex] != self[rightIndex] {
          return false
        }
        
        i += 1
        j -= 1
      }
      
      return true
    }
  }
  
  var longestPalindrome: SubSequence? {
    get {
      for i in stride(from: self.count-1, to: 1, by: -1) {
        for j in 0..<(self.count-1-i) {
          let startIndex = self.index(self.startIndex, offsetBy: j)
          let endIndex = self.index(self.startIndex, offsetBy: i+j)
          let substring = self[startIndex...endIndex]
          if substring.isPalindrome {
            return substring
          }
        }
      }
      
      return nil
    }
  }
}


//

extension String {
  func isAnagram(_ str: String) -> Bool {
    if self.count != str.count {
      return false
    }
    
    let s1 = self.sorted()
    let s2 = str.sorted()
    
    return s1 == s2
  }
  
  func substring(_ start: Int, _ end: Int) -> String {
    let startIndex = self.index(self.startIndex, offsetBy: start)
    let endIndex = self.index(self.startIndex, offsetBy: end)
    return String(self[startIndex..<endIndex])
  }
  
  var anagrammaticPairs: Int {
    get {
      var count = 0
      
      for length in 1..<self.count {
        for start1 in 0...self.count-length {
          let substr1 = substring(start1, start1 + length)
          for start2 in start1...self.count-length {
            if start1 == start2 { continue } 
            let substr2 = substring(start2, start2 + length)
            print("substr1: \(substr1), substr2: \(substr2)")
            if substr1.isAnagram(substr2) {
              count += 1
            }
          }
        }
      }
      
      return count
    }
  }
}

//

//extension StringProtocol {
//  var isSpecialPalindrome: Bool {
//    get {
//      var i = 0
//      var j = count-1
//      let char = self[startIndex]
//      
//      while i < j {
//        let leftIndex = index(startIndex, offsetBy: i)
//        let rightIndex = index(startIndex, offsetBy: j)
//        if self[leftIndex] != char || self[rightIndex] != char {
//          return false
//        }
//        
//        i += 1
//        j -= 1
//      }
//      
//      return true
//    }
//  }
//  
//  var specialPalindromicSubstrings: Int {
//    get {
//      var count = self.count
//      
//      for length in 2...self.count {
//        for start in 0...self.count-length {
//          let startIndex = index(self.startIndex, offsetBy: start)
//          let endIndex = index(self.startIndex, offsetBy: start+length)
//          let substr = self[startIndex..<endIndex]
//          
//          print(substr)
//          
//          if substr.isSpecialPalindrome {
//            count += 1
//          }
//        }
//      }
//      
//      return count
//    }
//  }
//}

struct CharacterPair: CustomStringConvertible {
  let character: Character
  var count: Int = 0
  
  init(character: Character, count: Int) {
    self.character = character
    self.count = count
  }
  
  var description: String {
    get {
      return "(\(character),\(count))"
    }
  }
}

extension StringProtocol {
  var characterPairs: [CharacterPair] {
    get {
      guard count > 0 else {
        return []
      }
      
      var res: [CharacterPair] = [CharacterPair(character: self.first!, count: 0)]
      
      for char in self {
        if char == res[res.count-1].character {
          res[res.count-1].count += 1
        } else {
          res.append(CharacterPair(character: char, count: 1))
        }
      }
      
      return res
    }
  }
  
  var specialPalindromicSubstrings: Int {
    get {
      var count = 0
      let pairs = characterPairs
      
      for (index, pair) in pairs.enumerated() {
        if pair.count == 1 {
          
          count += 1
          print(pair.character)
          
          if index > 0 && index < self.count-1 {
            let left = pairs[index-1]
            let right = pairs[index+1]
            
            if left.character == right.character {
              let minCount = Swift.min(left.count, right.count)
              count += minCount
              
              for i in 1...minCount {
                print("\(String(repeating: left.character, count: i))\(pair.character)\(String(repeating: right.character, count: i))")
              }
            }
          }
        } else {
          let combinations = pair.count*(pair.count+1)/2
          count += combinations
          
          print("\(String(repeating: pair.character, count: pair.count))")
        }
      }
      
      return count
    }
  }
}

extension Array {
  mutating func shuffle() {
    for i in 0..<count {
      swapAt(i, Int.random(in: 0..<count))
    }
  }
}

//

extension String {
  var firstRecurringCharacter: Character? {
    get {
      if self.count <= 1 {
        return nil
      }
      
      var characters = Set<Character>()
      
      for char in self {
        if characters.contains(char) {
          return char
        }
        
        characters.insert(char)
      }
      
      return nil
    }
  }
}


//

func containsSumOfTwo(_ array: [Int], _ k: Int) -> Bool {
  if array.count <= 1 {
    return false
  }
  
  var encountered = Set<Int>()
  
  for num in array {
    let complement = k - num
    if encountered.contains(complement) {
      return true
    }
    
    encountered.insert(num)
  }
  
  return false
}

//

func rotLeft(a: [Int], d: Int) -> [Int] {
  precondition(d <= a.count)
  precondition(d > 0)
  
  var result = a.dropFirst(d)
  let dropped = a.dropLast(a.count - d)
  result.append(contentsOf: dropped)
  return Array<Int>(result)
}
