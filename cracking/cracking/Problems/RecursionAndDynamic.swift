//
//  RecursionAndDynamic.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/16/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

// 8.1

func pathsToStep(_ n: Int) -> Int {
  var memo = Array<Int>(repeating: -1, count: n+1)
  return pathsToStep(n, &memo)
}

func pathsToStep(_ n: Int, _ memo: inout [Int]) -> Int {
  if n < 0 {
    return 0
  }
  
  if n == 0 {
    return 1
  }
  
  if memo[n] != -1 {
    return memo[n]
  }
  
  memo[n] = pathsToStep(n - 1, &memo) + pathsToStep(n - 2, &memo) + pathsToStep(n - 3, &memo)
  return memo[n]
}

// 8.3

func magic(array: [Int]) -> Int {
  var start = 0
  var end = array.count-1
  while start < end {
    let mid = start + (end - start) / 2
    
    if mid == array[mid] {
      return mid
    } else if mid > array[mid] {
      start = mid+1
    } else {
      end = mid
    }
  }
  
  return -1
}

// 8.4

func subsets(_ set: Set<Int>) -> Set<Set<Int>> {
  var result = Set<Set<Int>>()
  
  if set.count == 0 {
    result.insert(Set<Int>())
    return result
  }
  
  var remaining = set
  let first = remaining.removeFirst()
  
  for subset in subsets(remaining) {
    var withSet = subset
    let withoutSet = subset
    withSet.insert(first)
    
    result.insert(withSet)
    result.insert(withoutSet)
  }
  
  return result
}

// 8.6

struct TowerMove: Equatable {
  let from: Int
  let to: Int
  
  var reversed: TowerMove {
    get {
      return TowerMove(from: to, to: from)
    }
  }
}

struct Towers {
  private var stacks = Array<Stack<Int>>(repeating: Stack<Int>(), count: 3)
  private let numberOfDisks: Int
  private var previousMove: TowerMove? = nil
  
  init(numberOfDisks: Int) {
    self.numberOfDisks = numberOfDisks
    
    for i in stride(from: numberOfDisks, to: 0, by: -1) {
      stacks[0].push(i)
    }
  }
  
  var isComplete: Bool {
    get {
      return stacks[2].count == numberOfDisks
    }
  }
  
  var moves: [TowerMove] {
    get {
      var res: [TowerMove] = []
      
      for i in 0..<3 {
        for j in 0..<3 {
          if i != j {
            if let fromDisk = stacks[i].peek() {
              if let toDisk = stacks[j].peek() {
                if fromDisk <= toDisk {
                  res.append(TowerMove(from: i, to: j))
                }
              } else {
                res.append(TowerMove(from: i, to: j))
              }
            }
          }
        }
      }
      
      if let move = previousMove {
        res = res.filter({ $0 != move.reversed })
      }
      
      return res
    }
  }
  
  mutating func move(_ towerMove: TowerMove) {
    if let disk = stacks[towerMove.from].pop() {
      stacks[towerMove.to].push(disk)
      previousMove = towerMove
    }
  }
}

extension Towers: CustomStringConvertible {
  var description: String {
    get {
      return stacks.reduce("", { $0 + "\($1.description)\n" })
    }
  }
}

func play(game: Towers) -> Towers? {
  if game.moves.count == 0 {
    return nil
  }
  
  if game.isComplete {
    return game
  }
  
  for move in game.moves {
    var nextGame = game
    nextGame.move(move)
    print(nextGame)
    if let game = play(game: nextGame) {
      return game
    }
  }
  
  return nil
}


// 8.7 / 8.8

func permutations(_ str: String) -> Set<String> {
  if str.count <= 1 {
    return [str]
  }
  
  var res = Set<String>()
  for (index, char) in str.enumerated() {
    var substr = str
    substr.remove(at: index)
    let perms = permutations(substr)
    res = res.union(perms.map({ "\(char)" + $0 }))
  }
  
  return res
}

// 8.9

func validParens(_ n: Int) -> Set<String> {
  if n == 1 {
    return ["()"]
  }
  
  let prev = validParens(n-1)
  var res = Set<String>()
  
  for str in prev {
    res.insert("()" + str)
    res.insert(str + "()")
    res.insert("(" + str + ")")
  }
  
  return res
}

// 8.10

struct Color: Equatable {
  let r: Int
  let g: Int
  let b: Int
  let a: Int
  
  static var clear: Color {
    get {
      return Color(r: 0, g: 0, b: 0, a: 0)
    }
  }
  
  static var white: Color {
    get {
      return Color(r: 255, g: 255, b: 255, a: 255)
    }
  }
  
  static var black: Color {
    get {
      return Color(r: 0, g: 0, b: 0, a: 255)
    }
  }
  
  static var red: Color {
    get {
      return Color(r: 255, g: 0, b: 0, a: 255)
    }
  }
}

extension Color: CustomStringConvertible {
  var description: String {
    get {
      return String(format: "%.2f", Double(r + g + b) / Double(255*3)*Double(a)/255)
    }
  }
}

struct Point: Equatable {
  let x: Int
  let y: Int
  
  init(_ x: Int, _ y: Int) {
    self.x = x
    self.y = y
  }
  
  var nearby: [Point] {
    get {
      return [
        Point(x-1,y-1),
        Point(x,y-1),
        Point(x+1,y-1),
        Point(x-1,y),
        Point(x+1,y),
        Point(x-1,y+1),
        Point(x,y+1),
        Point(x+1,y+1),
      ]
    }
  }
}

struct Size {
  let width: Int
  let height: Int
  
  init(_ width: Int, _ height: Int) {
    self.width = width
    self.height = height
  }
  
  func contains(point: Point) -> Bool {
    return point.x >= 0 && point.x < width && point.y >= 0 && point.y < height
  }
}

class Canvas {
  private let size: Size
  private var grid: [[Color]]
  
  init(size: Size) {
    self.size = size
    self.grid = Array<[Color]>(repeating: Array<Color>(repeating: .clear, count: size.height), count: size.width)
  }
  
  func getColor(at point: Point) -> Color? {
    guard size.contains(point: point) else {
      return nil
    } 
    
    return grid[point.x][point.y]
  }
  
  func setColor(_ color: Color, at point: Point) {
    grid[point.x][point.y] = color
  }
  
  func paintFill(color: Color, point: Point) {
    if let oldColor = getColor(at: point) {
      paintFill(newColor: color, oldColor: oldColor, point: point)
    }
  }
  
  private func paintFill(newColor: Color, oldColor: Color, point: Point) {
    if !size.contains(point: point) || getColor(at: point) != oldColor {
      return
    }
    
    setColor(newColor, at: point)
    
    for neighbor in point.nearby {
      paintFill(newColor: newColor, oldColor: oldColor, point: neighbor)
    }
  }
}

extension Canvas: CustomStringConvertible {
  var description: String {
    get {
      var res = ""
      
      for i in 0..<size.width {
        for j in 0..<size.height {
          res += grid[i][j].description + " "
        }
        
        res += "\n\n"
      }
      
      return res
    }
  }
}


// 8.11 

func makeChange(_ total: Int, _ denoms: [Int], _ index: Int = 0) -> Int {
  if index >= denoms.count-1 {
    return total % denoms.last! == 0 ? 1 : 0
  }
  
  var ways = 0
  let den = denoms[index]
  let possibilities = total / den
  for i in 0...possibilities {
    ways += makeChange(total - i*den, denoms, index+1)
  }
  
  return ways
}

// 8.12

struct Coordinate {
  var r: Int
  var c: Int
  
  init(_ r: Int, _ c: Int) {
    self.r = r
    self.c = c
  }
}

extension Coordinate: CustomStringConvertible {
  var description: String {
    get {
      return "(\(r),\(c))"
    } 
  }
}

struct Board {
  enum TileType: String {
    case empty = "_"
    case queen = "q"
    case danger = "*"
  }
  
  private(set) var size: Int
  var tiles: [[TileType]]
  private(set) var nQueens = 0
  
  init(size: Int) {
    self.size = size
    tiles = Array<[TileType]>(repeating: Array<TileType>(repeating: .empty, count: size), count: size)
  }
  
  mutating func placeQueen(at coordinate: Coordinate) {
    tiles[coordinate.r][coordinate.c] = .queen
    
    for r in 0..<size {
      if tiles[r][coordinate.c] == .empty {
        tiles[r][coordinate.c] = .danger
      }
    }
    
    for c in 0..<size {
      if tiles[coordinate.r][c] == .empty {
        tiles[coordinate.r][c] = .danger
      }
    }
    
    for c in 0..<size {
      for r in 0..<size {
        let cDiff = c - coordinate.c
        let rDiff = r - coordinate.r
        
        if abs(cDiff) == abs(rDiff) {
          if tiles[r][c] == .empty {
            tiles[r][c] = .danger
          }
        }
      }
    }
    
    nQueens += 1
  }
  
  var empties: [Coordinate] {
    get {
      var res: [Coordinate] = []
      for r in 0..<size {
        for c in 0..<size {
          if tiles[r][c] == .empty {
            res.append(Coordinate(r, c))
          }
        }
      }
      
      return res
    }
  }
}

extension Board: CustomStringConvertible {
  var description: String {
    get {
      var res = ""
      
      for r in 0..<size {
        for c in 0..<size {
          res += tiles[r][c].rawValue + " "
        }
        
        res += "\n"
      }
      
      return res
    }
  }
}


func eightQueens(board: Board) {
  if board.nQueens == board.size {
    print(board)
    return
  }
  
  if board.empties.count == 0 {
    return
  }
  
  for coord in board.empties {
    var nextBoard = board
    nextBoard.placeQueen(at: coord)
    eightQueens(board: nextBoard)
  }
}

// 8.13

struct Box {
  let width: Int
  let height: Int
  let depth: Int
  
  init(_ w: Int, _ h: Int, _ d: Int) {
    width = w
    height = h
    depth = d
  }
}

extension Box: Comparable {
  static func <(lhs: Box, rhs: Box) -> Bool {
    return lhs.width < rhs.width &&
      lhs.height < rhs.height &&
      lhs.depth < rhs.depth
  }
}

func stack(boxes: [Box]) -> Int {
  var result = 0
  var allBottoms = boxes
  var memo = Array<Int>(repeating: -1, count: boxes.count)
  
  for i in 0..<boxes.count {
    let height = stack(&allBottoms, i, &memo)
    if height > result {
      result = height
    }
  }
  
  return result
}

private func stack(_ boxes: inout [Box], _ bottom: Int, _ memo: inout [Int]) -> Int {
  let bottomBox = boxes[bottom]
  let smallerBoxes = boxes.filter({ $0 < bottomBox })
  
  if smallerBoxes.count == 0 {
    return bottomBox.height
  }
  
  var result = 0
  
  if memo[bottom] != -1 {
    result = memo[bottom]
  } else {
    for i in 0..<smallerBoxes.count {
      let height = bottomBox.height + stack(&boxes, i, &memo)
      if height > result {
        result = height
      }
    }
  }
  
  memo[bottom] = result
  return result
}

// Extra

func LCS(_ parent1: String, _ parent2: String) -> Int {
  var p1 = parent1
  var p2 = parent2
  var memo: [[Int]] = Array<[Int]>(repeating: Array<Int>(repeating: -1, count: parent2.count), count: parent1.count)
  
  return LCS(&p1, &p2, p1.count-1, p2.count-1, &memo)
}

func LCS(_ parent1: inout String, _ parent2: inout String, _ length1: Int, _ length2: Int, _ memo: inout [[Int]]) -> Int {
  if length1 < 0 || length2 < 0 {
    return 0
  }
  
  let c1 = parent1[parent1.index(parent1.startIndex, offsetBy: length1)]
  let c2 = parent2[parent2.index(parent2.startIndex, offsetBy: length2)]
  
  var result = 0
  
  if memo[length1][length2] != -1 {
    result = memo[length1][length2]
  } else if c1 == c2 {
    result = 1 + LCS(&parent1, &parent2, length1-1,length2-1, &memo)
  } else {
    result = max( LCS(&parent1, &parent2, length1-1,length2, &memo), LCS(&parent1, &parent2, length1,length2-1, &memo) )
  }
  
  memo[length1][length2] = result
  return result
}

func hop(_ towers: [Int]) -> Bool {
  var memo = Array<Int>(repeating: -1, count: towers.count+1)
  var tow = towers
  return hop(&tow, 0, &memo)
}

func hop(_ towers: inout [Int], _ position: Int, _ memo: inout [Int]) -> Bool {
  if position >= towers.count {
    return true
  }
  
  if towers[position] == 0 {
    return false
  }
  
  var result = false
  
  if memo[position] != -1 {
    result = memo[position] == 0 ? false : true
  } else {
    let height = towers[position]
    for i in 1...height {
      if hop(&towers, position + i, &memo) {
        result = true
        break
      }
    }
  }
  
  memo[position] = result ? 1 : 0
  return result
}

func minhop(_ towers: [Int]) -> Int {
  var memo = Array<Int>(repeating: Int.max, count: towers.count+1)
  var tow = towers
  return minhop(&tow, 0, &memo)
}

func minhop(_ towers: inout [Int], _ position: Int, _ memo: inout [Int]) -> Int {
  if position >= towers.count {
    return 0
  }
  
  if towers[position] == 0 {
    return -1
  }
  
  var result = 0
  
  if memo[position] != Int.max {
    result = memo[position]
  } else {
    let height = towers[position]
    var shortest = minhop(&towers, position + 1, &memo)
    for i in 1...height {
      shortest = min(shortest, minhop(&towers, position + i, &memo))
    }
    
    result = 1 + shortest
  }
  
  memo[position] = result
  return result
}

//

func containsSum(_ array: [Int], _ k: Int) -> Bool {
  if array.count == 0 {
    return false
  }
  
  for (index, num) in array.enumerated() {
    let complement = k - num
    var remaining = array
    remaining.remove(at: index)
    
    if containsSum(remaining, complement) || complement == 0 {
      return true
    }
  }
  
  return false
}
