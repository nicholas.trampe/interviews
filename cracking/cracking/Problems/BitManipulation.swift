//
//  BitManipulation.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/16/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

// 5.1

func insert(n: Int, m: Int, i: Int, j: Int) -> Int {
  let left: Int = ~0 << j
  let right: Int = (1 << i) - 1
  let mask: Int = left | right
  let cleared = n & mask
  return cleared | (m << 2)
}

// 5.6

func numberOfBits(a: Int, b: Int) -> Int {
  var diff = a ^ b
  var count = 0  
  
  while diff != 0 {
    count += (diff & 1 != 0) ? 1 : 0
    diff = diff >> 1
  }
  
  return count
}

// 5.7

extension Int {
  mutating func pairWiseSwap() {
    var n = 0
    
    while n < 64 {
      
      let b1 = ((1 << n) & self) << 1
      let b2 = ((1 << (n+1)) & self) >> 1
      let c1 = ~(1<<n)
      let c2 = ~(1<<(n+1))
      
      self = self & c1
      self = self & c2
      self = self | b1
      self = self | b2
      
      n += 2
    }
  }
}

//

struct Screen {
  private typealias Position = (byte: Int, location: UInt8)
  private var bytes: [UInt8]
  private let width: Int
  private let height: Int
  
  init(_ width: Int, _ height: Int) {
    bytes = Array<UInt8>(repeating: 0, count: width*height)
    self.width = width
    self.height = height
  }
  
  mutating func set(_ pixel: Bool, _ x: Int, _ y: Int) {
    let pos = position(x, y)
    let mask: UInt8 = (1 << (7 - pos.location))
    
    bytes[pos.byte] &= ~mask
    
    if pixel {
      bytes[pos.byte] |= mask
    }
  }
  
  mutating func draw(_ x1: Int, _ y1: Int, _ x2: Int, _ y2: Int) {
    precondition(x1 >= 0 && x1 < width*8, "")
    precondition(x2 >= 0 && x2 < width*8, "")
    precondition(y1 >= 0 && y1 < height, "")
    precondition(y2 >= 0 && y2 < height, "")
    
    set(true, x1, y1)
    set(true, x2, y2)
  }
  
  func pixel(_ x: Int, _ y: Int) -> Bool {
    let pos = position(x, y)
    let mask: UInt8 = (1 << (7 - pos.location))
    
    return (bytes[pos.byte] & mask) != 0
  }
  
  private func position(_ x: Int, _ y: Int) -> Position {
    precondition(x >= 0 && x < width*8, "")
    precondition(y >= 0 && y < height, "")
    
    let byte = y*width + x / 8
    let location = UInt8(x % 8)
    
    return (byte, location)
  }
}

extension Screen: CustomStringConvertible {
  var description: String {
    get {
      var res = ""
      
      for r in 0..<height {
        for c in 0..<width*8 {
          let bit = pixel(c, r)
          res += "\(bit ? "0" : " ") "
        }
        
        res += "\n"
      }
      
      return res
    }
  }
}
