//
//  Hard.swift
//  cracking
//
//  Created by Nicholas Trampe on 9/6/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

// 17.7

func baby(names: [String:Int], synonyms: [String:String]) -> [String:Int] {
  var synmap: [String:[String]] = [:]
  var result: [String:Int] = [:]
  
  for (name, syn) in synonyms {
    synmap[name] = (synmap[name] ?? []) + [syn]
    synmap[syn] = (synmap[syn] ?? []) + [name]
  }
  
  for (name, count) in names {
    var found = false
    
    for (existingName, existingCount) in result {
      if isSynonym(start: name, end: existingName, synmap: synmap) {
        result[existingName] = existingCount + count
        found = true
        continue
      }
    }
    
    if !found {
      result[name] = count
    }
  }
  
  return result
}

func isSynonym(start: String, end: String, synmap: [String:[String]]) -> Bool {
  var queue = Queue<String>()
  var visited = Set<String>()
  
  queue.enqueue(start)
  
  while let name = queue.dequeue() {
    if name == end {
      return true
    }
    
    visited.insert(name)
    
    let neighbors = synmap[name]?.filter({ !visited.contains($0) }) ?? []
    for neighbor in neighbors {
      queue.enqueue(neighbor)
    }
  }
  
  return false
}

// 17.8

struct Performer {
  let height: Int
  let weight: Int
  
  init(_ height: Int, _ weight: Int) {
    self.height = height
    self.weight = weight
  }
}

extension Performer: Comparable {
  static func <(lhs: Performer, rhs: Performer) -> Bool {
    return lhs.height < rhs.height && lhs.weight < rhs.weight
  }
}

func tower(_ performers: [Performer]) -> Int {
  var memo = Array<Int>(repeating: -1, count: performers.count)
  var result = 0
  
  for bottom in 0..<performers.count {
    let height = tower(performers, bottom, &memo)
    if height > result {
      result = height
    }
  }
  
  return result
}

func tower(_ performers: [Performer], _ bottom: Int, _ memo: inout [Int]) -> Int {
  var result = 1
  
  if memo[bottom] != -1 {
    result = memo[bottom]
  } else {
    for (index, perf) in performers.enumerated() {
      if perf < performers[bottom] {
        let height = 1 + tower(performers, index, &memo)
        if height > result {
          result = height
        }
      }
    }
  }
  
  memo[bottom] = result
  return result
}


// 17.13

func unconcatenate(document: String, dictionary: Set<String>) -> [String] {
  var memo: [String:[String]] = [:]
  return unconcatenate(document: document, dictionary: dictionary, &memo)
}

func unconcatenate(document: String, dictionary: Set<String>, _ memo: inout [String:[String]]) -> [String] {
  if document.count <= 1 {
    return [document]
  }
  
  if dictionary.contains(document) {
    return [document]
  }
  
  if let result = memo[document] {
    return result
  }
  
  var result: [String] = []
  var minUnrecognizedCharacters = Int.max
  
  for i in 1..<document.count {
    let left = document.substring(0, i)
    let right = document.substring(i, document.count)
    
    var unrecognizedWords = 0
    var unrecognizedCharacters = 0
    var fitness = 0
    var words = left.count > 0 ? [left] : []
    
    words.append(contentsOf: unconcatenate(document: right, dictionary: dictionary, &memo))
    
    for word in words {
      if !dictionary.contains(word) {
        unrecognizedWords += 1
        unrecognizedCharacters += word.count
      }
    }
    
    fitness = unrecognizedWords * unrecognizedCharacters
    
    if fitness < minUnrecognizedCharacters {
      result = words
      minUnrecognizedCharacters = fitness
    }
  }
  
  memo[document] = result
  return result
}

// 17.12

class BiNode {
  public var node1: BiNode? = nil
  public var node2: BiNode? = nil
  public var data: Int = 0
  
  init(node1: BiNode?, node2: BiNode?, data: Int) {
    self.node1 = node1
    self.node2 = node2
    self.data = data
  }
}


extension BiNode {
  private func toList(startingAt node: BiNode?) -> BiNode? {
    guard let node = node else {
      return nil
    }
    
    var tail = toList(startingAt: node.node1)
    
    while let next = tail?.node2 {
      tail = next
    }
    
    node.node1 = tail
    
    node.node1?.node2 = node
    
    
    var head = toList(startingAt: node.node2)
    
    while let previous = head?.node1 {
      head = previous
    }
    
    node.node2 = head
    
    node.node2?.node1 = node
    
    return node
  }
  
  var toList: BiNode? {
    get {
      var node = toList(startingAt: self)
      
      while let previous = node?.node1 {
        node = previous
      }
      
      return node
    }
  }
}

extension BiNode {
  var treeDescription: String {
    get {
      return printTreeInOrder(startingAt: self) ?? ""
    }
  }
  
  var listDescription: String {
    get {
      return printListInOrder()
    }
  }
  
  private func printTreeInOrder(startingAt node: BiNode?) -> String? {
    guard let node = node else {
      return nil
    }
    
    var res = ""
    
    if let left = printTreeInOrder(startingAt: node.node1) {
      res += "\(left) "
    } 
    
    res += "\(node.data)"
    
    if let right = printTreeInOrder(startingAt: node.node2) {
      res += " \(right)"
    } 
    
    return res
  }
  
  private func printListInOrder() -> String {
    var current = self
    var res = "\(current.data) "
    
    while let next = current.node2 {
      res += "\(next.data) "
      current = next
    }
    
    return res
  }
}

// 17.15

func longestCombo(words: Set<String>) -> String {
  var result = ""
  
  for word in words {
    var others = words
    others.remove(word)
    if word.count > result.count {
      if isBuilt(word: word, from: others) {
        result = word
      }
    }
  }
  
  return result
}

func isBuilt(word: String, from others: Set<String>) -> Bool {
  if others.contains(word) {
    return true
  }
  
  for index in 0..<word.count {
    let prefix = word.substring(0,index)
    if others.contains(prefix) {
      let suffix = word.substring(index,word.count)
      return isBuilt(word: suffix, from: others)
    }
  }
  
  return false
}


// 17.18

func shortestSupersequence(shorter: Set<Int>, longer: [Int]) -> [Int] {
  var remaining = shorter
  var result: [Int] = longer
  
  for start in 0..<longer.count {
    remaining = shorter
    
    if remaining.contains(longer[start]) {
      remaining.remove(longer[start])
      
      var end = start+1
      
      while remaining.count > 0 && end < longer.count {
        if remaining.contains(longer[end]) {
          remaining.remove(longer[end])
        }
        end += 1
      }
      
      if remaining.count == 0 {
        if (end - start) < result.count {
          result = Array<Int>(longer[start..<end])
        }
      }
    }
  }
  
  return result
}


// 17.21

func volume(histogram: [Int]) -> Int {
  var index = 0
  
  for i in 0..<histogram.count {
    if histogram[i] > histogram[index] {
      index = i
    }
  }
  
  return volumeLeft(histogram: histogram, index: index) + 
    volumeRight(histogram: histogram, index: index)
}

func volumeLeft(histogram: [Int], index: Int) -> Int {
  if index <= 0 {
    return 0
  }
  
  var nextMaxIndex = 0
  var volume = 0
  
  for i in 0..<index {
    if histogram[i] > histogram[nextMaxIndex] {
      nextMaxIndex = i
    }
  }
  
  for i in nextMaxIndex..<index {
    volume += histogram[nextMaxIndex] - histogram[i]
  }
  
  return volume + volumeLeft(histogram: histogram, index: nextMaxIndex)
}

func volumeRight(histogram: [Int], index: Int) -> Int {
  if index >= histogram.count-1 {
    return 0
  }
  
  var nextMaxIndex = histogram.count-1
  var volume = 0
  
  for i in index+1..<histogram.count {
    if histogram[i] > histogram[nextMaxIndex] {
      nextMaxIndex = i
    }
  }
  
  for i in stride(from: nextMaxIndex, to: index, by: -1) {
    volume += histogram[nextMaxIndex] - histogram[i]
  }
  
  return volume + volumeRight(histogram: histogram, index: nextMaxIndex)
}


