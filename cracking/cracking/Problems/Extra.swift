//
//  Extra.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation


class Node {
  let name: String
  var previous: Node? = nil
  
  init(name: String, previous: Node?) {
    self.name = name
    self.previous = previous
  }
}

func path(network: [String:[String]], start: String, end: String) -> [String]? {
  var queue = Queue<Node>()
  var visited = Set<String>()
  queue.enqueue(Node(name: start, previous: nil))
  
  while let node = queue.dequeue() {
    if node.name == end {
      var result: [String] = [node.name]
      var currentNode = node
      
      while let previous = currentNode.previous {
        result.insert(previous.name, at: 0)
        currentNode = previous
      }
      
      return result
    }
    
    if !visited.contains(node.name) {
      visited.insert(node.name)
      
      if let neighbors = network[node.name] {
        for neighbor in neighbors {
          queue.enqueue(Node(name: neighbor, previous: node))
        }
      }
    }
  }
  
  return nil
}

//let network = [
//  "Min"     : ["William", "Jayden", "Omar"],
//  "William" : ["Min", "Noam"],
//  "Jayden"  : ["Min", "Amelia", "Ren", "Noam"],
//  "Ren"     : ["Jayden", "Omar"],
//  "Amelia"  : ["Jayden", "Adam", "Miguel"],
//  "Adam"    : ["Amelia", "Miguel", "Sofia", "Lucas"],
//  "Miguel"  : ["Amelia", "Adam", "Liam", "Nathan"],
//  "Noam"    : ["Nathan", "Jayden", "William"],
//  "Omar"    : ["Ren", "Min", "Scott"],
//]




struct CakeType {
  let weight: UInt
  let value: UInt
}

func maxDuffel(cakes: [CakeType], capacity: UInt) -> UInt {
  precondition(cakes.count > 0, "Must have cakes")
  
  var memo: [UInt:UInt] = [:]
  var cakeTypes = cakes
  return maxDuffel(&cakeTypes, capacity, &memo)
}

func maxDuffel(_ cakes: inout [CakeType], _ capacity: UInt, _ memo: inout [UInt:UInt]) -> UInt {
  if capacity <= 0 {
    return 0
  }
  
  var result: UInt = 0
  
  if let weight = memo[capacity] {
    result = weight
  } else {
    for cake in cakes {
      guard capacity >= cake.weight && cake.weight > 0 else { continue }
      
      let remainingWeight = capacity-cake.weight
      let cakeValue = cake.value + maxDuffel(&cakes, remainingWeight, &memo)
      
      if cakeValue > result {
        result = cakeValue
      }
    }
  }
  
  memo[capacity] = result
  return result
}


//

struct Matrix {
  private(set) var data: [[Bool]]
  
  init(_ rows: Int, _ columns: Int) {
    precondition(rows >= 1)
    precondition(columns >= 1)
    
    data = Array<Array<Bool>>(repeating: Array<Bool>(repeating: false, count: columns), count: rows)
  }
  
  public var width: Int {
    get {
      return data[0].count
    }
  }
  
  public var height: Int {
    get {
      return data.count
    }
  }
  
  public var area: Int {
    get {
      return width * height
    }
  }
  
  public var isSquare: Bool {
    get {
      return width == height
    }
  }
  
  public var isSingle: Bool {
    get {
      return area == 1
    }
  }
  
  public var isOnes: Bool {
    get {
      for r in 0..<data.count {
        for c in 0..<data[0].count {
          if get(r,c) != true {
            return false
          }
        }
      }
      
      return true
    }
  }
  
  public func get(_ row: Int, _ column: Int) -> Bool {
    precondition(row >= 0 && row < data.count)
    precondition(column >= 0 && column < data[0].count)
    
    return data[row][column]
  }
  
  public mutating func set(_ row: Int, _ column: Int, _ value: Bool) {
    precondition(row >= 0 && row < data.count)
    precondition(column >= 0 && column < data[0].count)
    
    data[row][column] = value
  }
  
  public func submatrix(_ startRow: Int, _ startColumn: Int, _ endRow: Int, _ endColumn: Int) -> Matrix {
    precondition(startRow >= 0 && startRow < data.count)
    precondition(startColumn >= 0 && startColumn < data[0].count)
    precondition(endRow >= 0 && endRow < data.count)
    precondition(endColumn >= 0 && endColumn < data[0].count)
    precondition(startRow <= endRow)
    precondition(startColumn <= endColumn)
    
    var matrix = Matrix((endRow - startRow + 1), endColumn - startColumn + 1)
    
    for r in startRow...endRow {
      for c in startColumn...endColumn {
        let newRow = r - startRow
        let newColumn = c - startColumn
        matrix.set(newRow,newColumn,get(r,c))
      }
    }
    
    return matrix
  }
}

extension Matrix: CustomStringConvertible {
  var description: String {
    get {
      var res = ""
      
      for r in 0..<data.count {
        for c in 0..<data[0].count {
          res += "\(get(r,c) ? "1" : "0") "
        }
        res += "\n"
      }
      
      return res
    }
  }
}

extension Matrix {
  static func random(rows: Int, columns: Int, density: Double = 0.5) -> Matrix {
    var matrix = Matrix(rows, columns)
    
    for r in 0..<rows {
      for c in 0..<columns {
        let set = (Double.random(in: 1...100) / 100) < density
        matrix.set(r, c, set)
      }
    }
    
    return matrix
  }
}

extension Matrix {
  public var largestArea: Int {
    get {
      var memo: [String:Int] = [:]
      return largestArea(0, 0, height-1, width-1, &memo)
    }
  }
  
  private func largestArea(_ startRow: Int, _ startColumn: Int, _ endRow: Int, _ endColumn: Int, _ memo: inout [String:Int]) -> Int {
    let key = "\(startRow),\(startColumn),\(endRow),\(endColumn)"
    
    if let area = memo[key] {
      return area
    }
    
    let matrix = submatrix(startRow, startColumn, endRow, endColumn)
    
    if matrix.isSingle {
      memo[key] = matrix.get(0,0) == true ? 1 : 0
      return matrix.get(0,0) == true ? 1 : 0
    }
    
    if matrix.isOnes {
      memo[key] = matrix.area
      return matrix.area
    }
    
    var result = 0
    let size = min(matrix.width, matrix.height) - (matrix.isSquare ? 2 : 1)
    let maxStartingRow = (matrix.height - size - 1)
    let maxStartingColumn = (matrix.width - size - 1)
    
    for r in 0...maxStartingRow {
      for c in 0...maxStartingColumn {
        let area = largestArea(r, c, r + size, c + size, &memo)
        
        if area > result {
          result = area
        }
      }
    }
    
    memo[key] = result
    return result
  }
}


//

extension String {
  var evaluated: Double {
    get {
      var numbers = Stack<Double>()
      var ops = Stack<Operator>()
      var currentIndex = 0
      var startOfNumberIndex = 0
      
      while currentIndex < self.count {
        let character = self[self.index(startIndex, offsetBy: currentIndex)]
        let op = Operator(rawValue: character)
        let numberStart = index(startIndex, offsetBy: startOfNumberIndex)
        let numberEnd = index(startIndex, offsetBy: currentIndex)
        
        if op != nil || currentIndex == self.count - 1 {
          if op != nil {
            if let number = Double(String(self[numberStart..<numberEnd])) {
              numbers.push(number)
              startOfNumberIndex = currentIndex + 1
            }
          } else if currentIndex == self.count - 1 {
            if let number = Double(String(self[numberStart...numberEnd])) {
              numbers.push(number)
            }
          }
          
          
          if let lastOp = ops.peek() {
            if lastOp == .multiply || lastOp == .divide {
              if let rhs = numbers.pop(),
                let lhs = numbers.pop() {
                let result = lastOp.operate(lhs, rhs)
                numbers.push(result)
              }
              
              ops.pop()
            }
          }
          
          
          if let op = op {
            ops.push(op)
          }
        }
        
        currentIndex += 1
      }
      
      var remainingOps = Stack<Operator>()
      var remainingNumbers = Stack<Double>()
      var result = 0.0
      
      while let op = ops.pop() {
        remainingOps.push(op)
      }
      
      while let number = numbers.pop() {
        remainingNumbers.push(number)
      }
      
      result = remainingNumbers.pop()!
      
      while let op = remainingOps.pop() {
        if let rhs = remainingNumbers.pop() {
          result = op.operate(result, rhs)
        }
      }
      
      return result
    }
  }
}

func possibleOperators(_ expression: String, _ target: Double) -> [String] {
  var memo: [String:[String]] = [:]
  return possibleOperators(expression, 1, target, &memo)
}

private let ops: [String] = [Operator.add, Operator.subtract, Operator.multiply].map({"\($0.rawValue)"}) + [""]
private func possibleOperators(_ expression: String, _ index: Int, _ target: Double, _ memo: inout [String:[String]]) -> [String] {
  let key = "\(expression)=\(index)"
  
  if index >= expression.count {
    if expression.evaluated == target {
      return [expression]
    }
    
    return []
  }
  
  var result: [String] = []
  let left = expression.substring(0, index)
  let right = expression.substring(index,expression.count)
  
  if let cachedResult = memo[key] {
    return cachedResult
  }
  
  for op in ops {
    let next = "\(left)\(op)\(right)"
    let nextIndex = index + (op.count == 0 ? 1 : 2)
    let expressions = possibleOperators(next, nextIndex, target, &memo)
    result += expressions
  }
  
  memo[key] = result
  return result
}

//

func getMaxProfit(_ stockPrices: [Int]) -> Int {
  guard stockPrices.count >= 2 else {
    return 0
  }
  
  var minPrice = stockPrices[0]
  var maxProfit = stockPrices[1] - stockPrices[0]
  
  for currentPrice in stockPrices[1...] {
    let potentialProfit = currentPrice - minPrice
    
    minPrice = min(minPrice, currentPrice)
    maxProfit = max(maxProfit, potentialProfit)
  }
  
  return maxProfit
}
