//
//  Trie.swift
//  trie
//
//  Created by Nicholas Trampe on 9/12/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

private class TrieNode {
  let character: Character
  private(set) var children: [Character:TrieNode] = [:]
  
  init(_ c: Character) {
    character = c
  }
  
  // Public API
  
  public var isCompleteWord: Bool {
    get {
      return children["*"] != nil
    }
  }
  
  public var isTerminating: Bool {
    get {
      return children.count == 0
    }
  }
  
  @discardableResult
  public func add(_ character: Character) -> TrieNode {
    precondition(children[character] == nil, "Attempting to add an already existing character")
    let node = TrieNode(character)
    children[character] = node
    return node
  }
  
  public func contains(_ character: Character) -> TrieNode? {
    return children[character] 
  }
}

extension TrieNode: Hashable {
  static func == (lhs: TrieNode, rhs: TrieNode) -> Bool {
    return lhs.character == rhs.character
  }
  
  var hashValue: Int {
    get {
      return character.hashValue
    }
  }
}

class Trie {
  private var root = TrieNode("*")
  
  // Public API
  
  init(_ dictionary: [String]) {
    for word in dictionary {
      add(word)
    }
  }
  
  init(_ file: String) {
    let components = file.split(separator: ".")
    precondition(components.count == 2, "Invalid filename")
    
    let name = String(components[0])
    let ext = String(components[1])
    
    if let dictionaryPath = Bundle.main.path(forResource: name, ofType: ext) {
      let dictionaryURL = URL(fileURLWithPath: dictionaryPath)
      if let fileText = try? String(contentsOf: dictionaryURL) {
        let words = fileText.split(separator: "\n")
        for word in words {
          add(String(word))
        }
      }
    }
  }
  
  public var words: [String] {
    get {
      return words(startingAt: root, partialWord: "")
    }
  }
  
  public func words(startingWith prefix: String) -> [String] {
    if prefix.count == 0 {
      return []
    }
    
    if let node = getNode(for: prefix) {
      return words(startingAt: node, partialWord: prefix)
    }
    
    return []
  }
  
  public func add(_ word: String) {
    var node: TrieNode = root
    
    for char in word {
      if let next = node.contains(char) {
        node = next
      } else {
        node = node.add(char)
      }
    }
    
    node.add("*")
  }
  
  public func contains(_ word: String) -> Bool {
    return getNode(for: word) != nil
  }
  
  // Private API
  
  private func getNode(for word: String) -> TrieNode? {
    var current: TrieNode = root
    
    for char in word {
      if let next = current.contains(char) {
        current = next
      } else {
        return nil
      }
    }
    
    return current
  }
  
  private func words(startingAt node: TrieNode, partialWord: String) -> [String] {
    if node.isTerminating {
      return []
    }
    
    var result: [String] = []
    
    if node.isCompleteWord {
      result.append(partialWord)
    }
    
    for (character, node) in node.children {
      let word = partialWord + "\(character)"
      
      result += words(startingAt: node, partialWord: word)
    }
    
    return result
  }
}

extension Trie: CustomStringConvertible {
  var description: String {
    get {
      return words.reduce("", { $0 + "\($1)\n" })
    }
  }
}
