//
//  Stack.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/14/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol Countable {
  var count: Int { get }
  var isEmpty: Bool { get }
}

protocol Stackable: Countable {
  associatedtype T
  mutating func pop() -> T?
  mutating func push(_ item: T)
  func peek() -> T?
}

struct Stack<T>: Stackable {
  private var elements: [T] = []
  
  @discardableResult
  mutating func pop() -> T? {
    guard !isEmpty else {
      return nil
    }
    
    return elements.removeLast()
  }
  
  mutating func push(_ item: T) {
    elements.append(item)
  }
  
  func peek() -> T? {
    return elements.last
  }
  
  var isEmpty: Bool {
    return elements.count == 0
  }
  
  var count: Int {
    get {
      return elements.count
    }
  }
}

extension Stack: CustomStringConvertible {
  var description: String {
    get {
      return elements.description
    }
  }
}
