//
//  Queue.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/14/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol Queueable: Countable {
  associatedtype T
  mutating func enqueue(_ item: T)
  mutating func dequeue() -> T?
  func peek() -> T?
}

struct Queue<T>: Queueable {
  private var elements: [T] = []
  
  mutating func enqueue(_ item: T) {
    elements.append(item)
  }
  
  @discardableResult
  mutating func dequeue() -> T? {
    guard !isEmpty else {
      return nil
    }
    
    return elements.removeFirst()
  }
  
  func peek() -> T? {
    return elements.first
  }
  
  var isEmpty: Bool {
    return count == 0
  }
  
  var count: Int {
    return elements.count
  }
}

extension Queue: CustomStringConvertible {
  var description: String {
    get {
      return elements.description
    }
  }
}

struct PriorityQueue<T> {
  private struct Element: Comparable {
    let value: T
    var priority = 0
    
    static func == (lhs: PriorityQueue<T>.Element, rhs: PriorityQueue<T>.Element) -> Bool {
      return lhs.priority == rhs.priority
    }
    
    static func < (lhs: Element, rhs: Element) -> Bool {
      return lhs.priority > rhs.priority
    }
  }
  
  private var elements: [Element] = []
  
  mutating func enqueue(_ item: T, with priority: Int) {
    var index = 0
    while index < elements.count && elements[index].priority > priority {
      index += 1
    }
    elements.insert(Element(value: item, priority: priority), at: index)
  }
  
  @discardableResult
  mutating func dequeue() -> T? {
    guard !isEmpty else {
      return nil
    }
    
    return elements.removeFirst().value
  }
  
  func peek() -> T? {
    return elements.first?.value
  }
  
  func updatePriority(for item: T, with priority: Int) {
    
  }
  
  var isEmpty: Bool {
    return count == 0
  }
  
  var count: Int {
    return elements.count
  }
}

extension PriorityQueue: CustomStringConvertible {
  var description: String {
    get {
      return elements.description
    }
  }
}
