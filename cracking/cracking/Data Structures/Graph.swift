//
//  Graph.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/14/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

class Vertex<T: Hashable>: Hashable {
  let data: T
  var visited: Bool = false
  
  init(_ data: T) {
    self.data = data
  }
  
  var hashValue: Int {
    get {
      return data.hashValue
    }
  }
  
  static func == (lhs: Vertex<T>, rhs: Vertex<T>) -> Bool {
    return lhs === rhs
  }
}

struct Edge<T: Hashable>: Hashable {
  let from: Vertex<T>
  let to: Vertex<T>
  let weight: Int
  
  init(from: Vertex<T>, to: Vertex<T>, weight: Int = 1) {
    self.from = from
    self.to = to
    self.weight = weight
  }
}

protocol Graph: CustomStringConvertible {
  associatedtype Element: Hashable
  var verticies: [Vertex<Element>] { get }
  var edges: [Edge<Element>] { get }
  
  @discardableResult
  func add(element: Element) -> Vertex<Element>
  func addDirectedEdge(from: Vertex<Element>, to: Vertex<Element>, weight: Int)
  func addUnDirectedEdge(from: Vertex<Element>, to: Vertex<Element>, weight: Int)
  func removeEdge(from: Vertex<Element>, to: Vertex<Element>)
  func removeOutgoingEdges(from: Vertex<Element>)
  func removeIncomingEdges(to: Vertex<Element>)
  
  func edges(from: Vertex<Element>) -> [Edge<Element>]
  func edge(from: Vertex<Element>, to: Vertex<Element>) -> Int?
  func neighbors(from: Vertex<Element>) -> [Vertex<Element>]
  
  @discardableResult
  func bfs(start: Vertex<Element>, _ check: (Vertex<Element>) -> (Bool)) -> Bool
  
  @discardableResult
  func dfs(start: Vertex<Element>, _ check: (Vertex<Element>) -> (Bool)) -> Bool
}

extension Graph {
  
  private func clearVisited() {
    verticies.forEach({ $0.visited = false })
  }
  
  @discardableResult
  func bfs(start: Vertex<Element>, _ check: (Vertex<Element>) -> (Bool)) -> Bool {
    clearVisited()
    
    var queue = Queue<Vertex<Element>>()
    queue.enqueue(start)
    
    while !queue.isEmpty {
      let node = queue.dequeue()!
      node.visited = true
      
      if check(node) {
        return true
      }
      
      let next = neighbors(from: node).filter({ $0.visited == false })
      next.forEach {
        $0.visited = true
        queue.enqueue($0)
      }
    }
    
    return false
  }
  
  @discardableResult
  func dfs(start: Vertex<Element>, _ check: (Vertex<Element>) -> (Bool)) -> Bool {
    clearVisited()
    
    return dfs(startingAt: start, check)
  }
  
  private func dfs(startingAt node: Vertex<Element>, _ check: (Vertex<Element>) -> Bool) -> Bool {
    node.visited = true
    if check(node) {
      return true
    }
    
    let next = neighbors(from: node).filter({ $0.visited == false })
    for neighbor in next {
      if dfs(startingAt: neighbor, check) {
        return true
      }
    }
    
    return false
  }
}

class AdjacencyListGraph<T: Hashable>: Graph {
  typealias Element = T
  
  var verticies: [Vertex<T>] = []
  var edges: [Edge<T>] = []
  
  @discardableResult
  func add(element: T) -> Vertex<T> {
    precondition(!verticies.map({$0.data}).contains(element), "Attemping to insert duplicate vertex")
    
    let vertex = Vertex<T>(element)
    verticies.append(vertex)
    return vertex
  }
  
  func addDirectedEdge(from: Vertex<T>, to: Vertex<T>, weight: Int) {
    edges.append(Edge(from: from, to: to, weight: weight))
  }
  
  func addUnDirectedEdge(from: Vertex<T>, to: Vertex<T>, weight: Int) {
    edges.append(Edge(from: from, to: to, weight: weight))
    edges.append(Edge(from: to, to: from, weight: weight))
  }
  
  func removeEdge(from: Vertex<Element>, to: Vertex<Element>) {
    edges.removeAll(where: {
      $0.from === from && $0.to === to
    })
  }
  
  func removeOutgoingEdges(from: Vertex<Element>) {
    edges.removeAll(where: {
      $0.from === from
    })
  }
  
  func removeIncomingEdges(to: Vertex<Element>) {
    edges.removeAll(where: {
      $0.to === to
    })
  }
  
  func edges(from: Vertex<T>) -> [Edge<T>] {
    return self.edges.filter({ $0.from == from })
  }
  
  func edge(from: Vertex<Element>, to: Vertex<Element>) -> Int? {
    for edge in edges {
      if edge.from === from && edge.to === to {
        return edge.weight
      }
    }
    
    return nil
  }
  
  func neighbors(from: Vertex<T>) -> [Vertex<T>] {
    return edges(from: from).map({ $0.to })
  }  
}

extension AdjacencyListGraph: CustomStringConvertible {
  var description: String {
    get {
      var res = ""
      
      verticies.forEach({ vertex in 
        let nearby = neighbors(from: vertex)
        res += "\(vertex.data): \(nearby.map({$0.data}))\n"
      })
      
      return res
    }
  }
}

class AdjacencyMatrixGraph<T: Hashable>: Graph {
  typealias Element = T
  
  var verticies: [Vertex<T>] = []
  var edges: [Edge<T>] {
    get {
      var res: [Edge<T>] = []
      
      for i in 0..<verticies.count {
        for j in 0..<verticies.count {
          if matrix[i][j] != 0 {
            res.append(Edge<T>(from: verticies[i], to: verticies[j], weight: matrix[i][j]))
          }
        }
      }
      
      return res
    }
  }
  
  //// [from][to]
  private var matrix: [[Int]] = []
  
  @discardableResult
  func add(element: T) -> Vertex<T> {
    precondition(!verticies.map({$0.data}).contains(element), "Attemping to insert duplicate vertex")
    
    
    let vertex = Vertex<T>(element)
    verticies.append(vertex)
    var updatedMatrix: [[Int]] = Array<[Int]>(repeating: Array<Int>(repeating: 0, count: verticies.count), count: verticies.count)
    
    for i in 0..<matrix.count {
      for j in 0..<matrix.count {
        updatedMatrix[i][j] = matrix[i][j]
      }
    }
    
    matrix = updatedMatrix
    
    return vertex
  }
  
  func addDirectedEdge(from: Vertex<T>, to: Vertex<T>, weight: Int) {
    guard let fromIndex = verticies.index(of: from),
          let toIndex = verticies.index(of: to) else {
        preconditionFailure("Verticies not found")
    }
    matrix[fromIndex][toIndex] = weight
  }
  
  func addUnDirectedEdge(from: Vertex<T>, to: Vertex<T>, weight: Int) {
    guard let fromIndex = verticies.index(of: from),
          let toIndex = verticies.index(of: to) else {
        preconditionFailure("Verticies not found")
    }
    
    matrix[fromIndex][toIndex] = weight
    matrix[toIndex][fromIndex] = weight
  }
  
  func removeEdge(from: Vertex<Element>, to: Vertex<Element>) {
    guard let fromIndex = verticies.index(of: from),
          let toIndex = verticies.index(of: to) else {
        preconditionFailure("Verticies not found")
    }
    
    matrix[fromIndex][toIndex] = 0
  }
  
  func removeOutgoingEdges(from: Vertex<Element>) {
    guard let fromIndex = verticies.index(of: from) else {
        preconditionFailure("Vertex not found")
    }
    
    for i in 0..<matrix.count {
      matrix[fromIndex][i] = 0
    }
  }
  
  func removeIncomingEdges(to: Vertex<Element>) {
    guard let toIndex = verticies.index(of: to) else {
      preconditionFailure("Vertex not found")
    }
    
    for i in 0..<matrix.count {
      matrix[i][toIndex] = 0
    }
  }
  
  func edges(from: Vertex<T>) -> [Edge<T>] {
    guard let fromIndex = verticies.index(of: from) else {
        preconditionFailure("Vertex not found")
    }
    
    var res: [Edge<T>] = []
    
    matrix[fromIndex].enumerated().forEach { (toIndex, edgeWeight) in
      if edgeWeight != 0 {
        res.append(Edge(from: verticies[fromIndex], to: verticies[toIndex], weight: edgeWeight))
      }
    }
    
    return res
  }
  
  func edge(from: Vertex<Element>, to: Vertex<Element>) -> Int? {
    guard let fromIndex = verticies.index(of: from),
          let toIndex = verticies.index(of: to) else {
        preconditionFailure("Verticies not found")
    }
    
    let weight = matrix[fromIndex][toIndex]
    
    return weight != 0 ? weight : nil
  }
  
  func neighbors(from: Vertex<T>) -> [Vertex<T>] {
    return edges(from: from).map({ $0.to })
  }  
}

extension AdjacencyMatrixGraph: CustomStringConvertible {
  var description: String {
    get {
      var res = "  \(verticies.reduce("", { $0 + "\($1.data) "}))\n"
      
      for i in 0..<verticies.count {
        for j in 0...verticies.count {
          if j == 0 {
            res += "\(verticies[i].data) "
          } else {
            res += "\(matrix[i][j-1]) "
          }
        }
        
        res += "\n"
      }
      
      return res
    }
  }
}

//class AdjacencyMatrixGraph<T: Hashable>: Graph {  
//  
//  var nodes: [Node] = []
//  var edges: [Edge] = []
//  
//  func add(node: Node) {
//    nodes.append(node)
//  }
//  
//  func connect(node1: Node, node2: Node) {
//    let edge = Edge(from: node1, to: node2)
//    node1.neighbors.append(node2)
//  }
//  
//  func bfs(root: Node, _ check: (Node) -> (Bool)) -> Bool {
//    nodes.forEach({ $0.visited = false })
//    
//    var queue = Queue<Node>()
//    queue.enqueue(root)
//    
//    while !queue.isEmpty {
//      let node = queue.dequeue()!
//      node.visited = true
//      
//      if check(node) {
//        return true
//      }
//      
//      let neighbors = node.neighbors.filter({ !$0.visited })
//      neighbors.forEach { queue.enqueue($0) }
//    }
//    
//    return false
//  }
//}
//
//extension Graph.Node: CustomStringConvertible {
//  var description: String {
//    get {
//      return "\(data): \(neighbors.map({$0.data}))"
//    }
//  }
//}
//
//extension Graph: CustomStringConvertible {
//  var description: String {
//    get {
//      var res = ""
//      
//      nodes.forEach({
//        res += "\($0)\n"
//      })
//      
//      return res
//    }
//  }
//}
