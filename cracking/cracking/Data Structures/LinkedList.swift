//
//  LinkedList.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/14/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

class List<T> {
  class ListNode<T> {
    var data: T
    var previous: ListNode<T>? = nil
    var next: ListNode<T>? = nil
    
    init(data: T) {
      self.data = data
    }
  }
  
  private(set) var head: ListNode<T>? = nil
  
  var tail: ListNode<T>? {
    get {
      guard var node = head else {
        return nil
      }
      
      while let next = node.next {
        node = next
      }
      
      return node
    }
  }
  
  func append(_ element: T) {
    let node = ListNode<T>(data: element)
    
    if head == nil {
      head = node
    } else {
      node.previous = tail
      tail?.next = node
    }
  }
}

extension List: CustomStringConvertible {
  var description: String {
    get {
      guard var node = head else {
        return "[]"
      }
      
      var res = "[\(node.data), "
      
      while let next = node.next {
        res += "\(next.data)" + ", "
        node = next
      }
      
      res = String(res.dropLast(2) + "]")
      
      return res
    }
  }
}
