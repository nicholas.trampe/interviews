//
//  HashTable.swift
//  cracking
//
//  Created by Nicholas Trampe on 8/14/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct HashTable<Key: Hashable, Value> {
  private typealias Element = (key: Key, value: Value)
  private typealias List = [Element]
  private var lists: [List] = Array<List>(repeating: [], count: 100)
  private(set) var count: Int = 0
  
  public subscript(key: Key) -> Value? {
    get {
      return value(for: key)
    }
    
    set {
      if let value = newValue {
        update(at: key, value: value)
      } else {
        delete(at: key)
      }
    }
  }
  
  private mutating func update(at key: Key, value: Value) {
    let listIndex = index(of: key)
    
    for (elementIndex, element) in lists[listIndex].enumerated() {
      if element.key == key {
        lists[listIndex][elementIndex].value = value
        return
      }
    }
    
    lists[listIndex].append((key: key, value: value))
    count += 1
  }
  
  private mutating func delete(at key: Key) {
    let listIndex = index(of: key)
    
    for (elementIndex, element) in lists[listIndex].enumerated() {
      if element.key == key {
        lists[listIndex].remove(at: elementIndex)
        count -= 1
        return
      }
    }
  }
  
  private func value(for key: Key) -> Value? {
    let list = lists[index(of: key)]
    var element: Element? = nil
    var count = 0
    
    while element?.key != key && count < list.count {
      element = list[count]
      count += 1
    }
    
    return element?.value
  }
  
  private func index(of key: Key) -> Int {
    return abs(key.hashValue) % lists.count
  }
}
