//
//  BinarySearchTree.swift
//  cracking
//
//  Created by Nicholas Trampe on 9/12/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

class BinarySearchTree<T: Comparable> {
  let data: T
  var left: BinarySearchTree? = nil
  var right: BinarySearchTree? = nil
  var parent: BinarySearchTree? = nil
  
  init(_ data: T) {
    self.data = data
  }
  
  init?(_ array: [T]) {
    if array.count == 0 {
      return nil
    }
    
    if array.count == 1 {
      data = array[0]
      return
    }
    
    let elements = array.sorted()
    
    let middleIndex = elements.count / 2
    let leftElements = Array<T>(elements[0..<middleIndex])
    let rightElements = Array<T>(elements[(middleIndex+1)...])
    
    data = elements[middleIndex]
    left = BinarySearchTree(leftElements)
    right = BinarySearchTree(rightElements)
  }
  
  func insert(_ element: T) {
    if element < data {
      if let left = left {
        left.insert(element)
      } else {
        left = BinarySearchTree(element)
        left?.parent = self
      }
    } else {
      if let right = right {
        right.insert(element)
      } else {
        right = BinarySearchTree(element)
        right?.parent = self
      }
    }
  }
  
  func find(_ data: T) -> BinarySearchTree? {
    if self.data == data {
      return self
    }
    
    if data < self.data {
      if let left = left {
        return left.find(data)
      } else {
        return nil
      }
    } else {
      if let right = right {
        return right.find(data)
      } else {
        return nil
      }
    }
  }
  
  func find(_ node: BinarySearchTree) -> Bool {
    if self === node {
      return true
    }
    
    if node.data < self.data {
      if let left = left {
        return left.find(node)
      } else {
        return false
      }
    } else {
      if let right = right {
        return right.find(node)
      } else {
        return false
      }
    }
  }
  
  var preorder: [T] {
    get {
      return [data] + (left?.preorder ?? []) + (right?.preorder ?? [])
    }
  }
  
  var postorder: [T] {
    get {
      return (left?.postorder ?? []) + (right?.postorder ?? []) + [data]
    }
  }
  
  var inorder: [T] {
    get {
      return (left?.inorder ?? []) + [data] + (right?.inorder ?? [])
    }
  }
}


extension BinarySearchTree: CustomStringConvertible {
  var description: String {
    get {
      return "\(inorder)"
    }
  }
}
